package com.sks.asbtracts;

/**
 * Abstract Class of concrete Discount
 */
public abstract class DecoratorOfBasicDiscount extends Component{
    protected Component component;
    protected int applyOnProductId;

    ///// contructors
    protected DecoratorOfBasicDiscount(){};

    protected DecoratorOfBasicDiscount(Component component) {
        //TODO : check that component is a product, not a discount item
        this.component=component;
        this.applyOnProductId=component.getId();
    }


    ///// getters
    public Component getComponent() {
        return component;
    }
    public int getApplyOnProductId() {
        return applyOnProductId;
    }


    ///// setters
    public void setComponent(Component component) {
        this.component = component;
    }
    public void setApplyOnProductId(int applyOnProductId) {
        this.applyOnProductId = applyOnProductId;
    }


    // abstract make new definition mandatory for sub classes
    public abstract String getName();


    // abstract make new definition mandatory for sub classes
    public abstract double getPrice();
}
