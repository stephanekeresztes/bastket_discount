package com.sks.asbtracts;

import com.sks.utils.Constants;
import java.text.NumberFormat;

/**
 * Abstract Class of components : Product and Discounts
 */
public abstract class Component {

    private int id;
    private String name;
    private double price;


    ///// Constructors
    public Component(){};

    public Component (String name, Double price, int id) {
       this.setName(name);
       this.setPrice(price);
       this.setId(id);
    }


    ///// getters
    public String getName() { return name;  }
    public double getPrice() { return price; };
    public int getId() { return id; };


    ///// setters
    private void setName(String name) { this.name = name; }
    private void setPrice(double price) { this.price = price; }
    private void setId(int id) { this.id = id; }

    /**
     *
     * @return String to display attributes
     */
    public String displayDetails() {
        NumberFormat format=NumberFormat.getInstance();
        format.setMinimumFractionDigits(Constants.MAX_DIGIT);
        return this.id+Constants.TAB+this.name+Constants.SEPARATOR+format.format(getPrice())+"€";
    }

}
