package com.sks.executions;

import com.sks.classes.*;

public class Main {

    private StoreProductList storeProductList=null; // list of products available in store
    private BasicDiscountList basicDiscountList=null; //list of basic discounts available today in store
    private CustomerItemsList customerItemsList =null; //list of product item select by customer

    public static void main(String[] args) {
        System.out.println("begin");

        Main main=new Main();
        main.createNewProductsList();
        main.createBasicDiscountList();

        main.simulateCustomerschoices(main.storeProductList);
        main.checkout();





        System.out.println("end, thank you");
    }

    /**
     * initialize available products in store
     */
    public void createNewProductsList(){
        //create list
        this.storeProductList=new StoreProductList();
        Product product_milk = new Product("Lactel 1 litre", 1.20,1, "product_milk");
        this.storeProductList.addItemToProductList(product_milk);
        Product product_pen =new Product("Blue pen",2.0,2, "product_pen");
        this.storeProductList.addItemToProductList(product_pen);
        Product product_phone =new Product("Google phone",450.0,3, "product_phone");
        this.storeProductList.addItemToProductList(product_phone);
        Product product_potatoes =new Product("Fresh potatoes / pound",0.5,4, "product_potatoes");
        this.storeProductList.addItemToProductList(product_potatoes);
        Product product_cucumber =new Product("cucumber / unit",0.5,5,"product_cucumber");
        this.storeProductList.addItemToProductList(product_cucumber);
        Product product_glass =new Product("glass / unit",3.0,6, "product_glass");
        this.storeProductList.addItemToProductList(product_glass);

        //display list
        this.storeProductList.printStoreProductList();
    }


    /**
     * initialize available basic discount in store : set id of the product linked with discount and the % off
     */
    public void createBasicDiscountList() {
        //create list
        this.basicDiscountList=new BasicDiscountList();
        OneItemPercentOff percent10 = new OneItemPercentOff(storeProductList.searchComponentByIdInProductList(2),10.0);
        this.basicDiscountList.addItem(percent10);
        OneItemPercentOff percent20 = new OneItemPercentOff(storeProductList.searchComponentByIdInProductList(3),20.0);
        this.basicDiscountList.addItem(percent20);

        //display list
        this.basicDiscountList.printBasicDiscountList();
    }


    /**
     * simulate Customer's choices by setting set quantity and id of product
     */
    public void simulateCustomerschoices(StoreProductList storeProductList){

        customerItemsList =new CustomerItemsList();
        customerItemsList.addItemToCustomerList(new CustomerItem(2,storeProductList.searchComponentByIdInProductList(1)));
        customerItemsList.addItemToCustomerList(new CustomerItem(1,storeProductList.searchComponentByIdInProductList(2)));
        customerItemsList.addItemToCustomerList(new CustomerItem(1,storeProductList.searchComponentByIdInProductList(3)));

        customerItemsList.printList();
    }

    /**
     * checkout and print final bill
     */
    public void checkout(){
        Checkout checkout= new Checkout();
        checkout.loopCustomerListToApplyBasicDiscount(customerItemsList,basicDiscountList,storeProductList );

        checkout.printFinalBill();

    }

    }
