package com.sks.classes;


import com.sks.asbtracts.DecoratorOfBasicDiscount;
import com.sks.utils.Constants;
import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * this class apply or not discount on each product choosen by the customer and calculate the final price
 */
public class Checkout {

    ArrayList<BillLine> billArrayList; // final bill contains BillLines items

    public Checkout () {
        this.billArrayList=new ArrayList<>(10);
    }

    //loop on customer's product list and apply basic discount to fill bill
    public void loopCustomerListToApplyBasicDiscount(CustomerItemsList customerItemsList, BasicDiscountList basicDiscountList, StoreProductList storeProductList) {
        for (CustomerItem ci : customerItemsList.getCustomerList()) {
            //intialize a new billLine instance
            BillLine billLine=null;

            //get id product
            int id=ci.getProductId();

            //search if a DecoratorOfBasicDiscount instance has to be applied to current the product id
            DecoratorOfBasicDiscount decoratorOfBasicDiscount=null;
            try {
                //TODO : what if multiple discounts have to be applied to product ?
                decoratorOfBasicDiscount = basicDiscountList.getBasicDiscountList().stream().filter(e -> e.getApplyOnProductId() == id).findFirst().get();
            } catch ( NoSuchElementException e) {
                //No discount have been found for the current item. Job done, nothing else to do here
            } finally {
                //nothing to do
            }

            if (decoratorOfBasicDiscount!=null) {
                //case of  : found discount, have to be applied
                decoratorOfBasicDiscount.setComponent(ci.getProductInstance());
                double finalPrice=decoratorOfBasicDiscount.getPrice()*ci.getQuantity();
                billLine=new BillLine(ci.getProductId(),ci.getQuantity(),ci.getProductName(), "discount "+decoratorOfBasicDiscount.getName(), finalPrice);
                billArrayList.add(billLine);
            } else {
                //case of  : no discount to applied
                //to get final price, apply quantity to original price
                double finalPrice=storeProductList.searchComponentByIdInProductList(id).getPrice()*ci.getQuantity();
                billLine=new BillLine(ci.getProductId(),ci.getQuantity(),ci.getProductName(), "(no discount)", finalPrice);
                //add new Bill line to billArrayList
                billArrayList.add(billLine);

            }



        }
    }

    /**
     *
     */
    //TODO  method to loop on customer's list to apply complex discount


    /**
     * display final bill
     */
    public void printFinalBill() {
        System.out.println(Constants.LINE_SEPARATOR);
        System.out.println(Constants.SKIP_LINE+"Final bill"+ Constants.SKIP_LINE);
        if (this.billArrayList!=null) {
            this.billArrayList.forEach(e -> System.out.println(e.printBillLine()));
        } else {
            System.out.println("empty customer list, nothing to display");
        }

        double mySum=billArrayList.stream().mapToDouble(value -> value.getFinalPrice()).sum();
        System.out.println(mySum+"€ Total ");
    }

}
