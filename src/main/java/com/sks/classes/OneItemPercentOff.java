package com.sks.classes;

import com.sks.asbtracts.Component;
import com.sks.asbtracts.DecoratorOfBasicDiscount;


/**
 * class for simple discount with %
 */
public class OneItemPercentOff extends DecoratorOfBasicDiscount {

    private double percent;

    // Constructeur qui prend en paramètre le Component Product sur lequel s'applique, la remise, et la remise en %
    public OneItemPercentOff(Component component, Double percent)
    {
        this.component=component;
        this.percent=percent;
        this.applyOnProductId=component.getId();
    }

    public int getApplyOnProductId() {
        return applyOnProductId;
    }
    public double getPercent() {
        return percent;
    }


    public void setApplyOnProductId(int applyOnProductId) {
        this.applyOnProductId = applyOnProductId;
    }
    public void setPercent(double percent) {
        this.percent = percent;
    }


    @Override
    public String getName() {
        return component.getName()+" save "+percent+"% off";
    }

    @Override
    public double getPrice() {
        return component.getPrice()*(1-percent/100);
    }
}
