package com.sks.classes;

import com.sks.asbtracts.Component;

import java.util.ArrayList;

/**
 * a line of customer list
 */
public class CustomerItem {
    private int productId;
    private int quantity; // can be number of units or weight
    private String productName;
    private Component productInstance;

    public CustomerItem(int quantity, Component productInstance) {
        this.productId=productInstance.getId();
        this.quantity=quantity;
        this.productName=productInstance.getName();
        this.productInstance=productInstance;
    }

    public int getProductId() { return productId;  }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Component getProductInstance() { return productInstance; }

    public void setProductInstance(Component productInstance) { this.productInstance = productInstance;  }
}
