package com.sks.classes;

import com.sks.asbtracts.Component;
import com.sks.utils.Constants;

import java.util.ArrayList;
import java.util.NoSuchElementException;


/**
 * list of products available in store
 */
public class StoreProductList {

    private ArrayList<Component> productsList=null; // product list of the store

    public StoreProductList(){
        this.productsList=new ArrayList<>(10);
    }

    public ArrayList<Component> getProductsList() {
        return productsList;
    }

    public void setProductsList(ArrayList<Component> productsList) {
        this.productsList = productsList;
    }

    /**
     *
     * @param component
     */
    public void addItemToProductList(Component component) {
        //TODO : check that component is not null;
        //TODO : check that product id doesn't already exists
        this.productsList.add(component);
    }

    /**
     *
     * @param id
     * @return
     */
    public Component searchComponentByIdInProductList(int id) {
        Component component=null;
        try {
            component=productsList.stream().filter(e -> e.getId() == id).findFirst().get();
        } catch (NoSuchElementException e) {
            System.out.println("the product "+id+ " doesn't exist");
            System.exit(100);
        } finally {
            return component;
        }
    }

    /**
     * print list of products available in store
     */
    public void printStoreProductList(){
        System.out.println(Constants.LINE_SEPARATOR);
        System.out.println(Constants.SKIP_LINE+"Product list"+ Constants.SKIP_LINE);
        productsList.stream().forEach(e-> System.out.println(e.displayDetails()));
    }
}
