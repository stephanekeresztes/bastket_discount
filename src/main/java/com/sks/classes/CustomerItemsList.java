package com.sks.classes;

import com.sks.asbtracts.Component;
import com.sks.utils.Constants;

import java.util.ArrayList;

public class CustomerItemsList {

    ArrayList<CustomerItem> customerListItems;

    public CustomerItemsList(){
        this.customerListItems=new ArrayList<>(10);
    }

    public ArrayList<CustomerItem> getCustomerList() {
        return customerListItems;
    }

    public void setCustomerList(ArrayList<CustomerItem> customerList) {
        this.customerListItems = customerList;
    }

    public void addItemToCustomerList(CustomerItem customerItem) {
        //TODO : check that customerItem is not null;
        //TODO : check that product id is not already in list
        this.customerListItems.add(customerItem);
    }

    /**
     * print customer's list
     */
    public void printList(){
        System.out.println(Constants.LINE_SEPARATOR);
        System.out.println(Constants.SKIP_LINE+"Customer list"+ Constants.SKIP_LINE);
        customerListItems.stream().forEach(e-> System.out.println("id="+e.getProductId()+Constants.SPACE_SMALL+"quantity="+e.getQuantity()
        + Constants.SPACE_SMALL+e.getProductName()));
    }
}
