package com.sks.classes;

import com.sks.utils.Constants;


/**
 * this class handle a line of the final of bill
 */
public class BillLine {

    private int productId;
    private int quantity; // can be number of units or weight
    private String productName;
    private String discountMessage;
    private double finalPrice;

    public BillLine() {
    }

    public BillLine(int productId, int quantity, String productName, String discountMessage, double finalPrice) {
        this.productId = productId;
        this.quantity = quantity;
        this.productName = productName;
        this.discountMessage = discountMessage;
        this.finalPrice = finalPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDiscountMessage() {
        return discountMessage;
    }

    public void setDiscountMessage(String discountMessage) {
        this.discountMessage = discountMessage;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String printBillLine() {
        String ret=this.finalPrice+"€"+Constants.SPACE_SMALL
                +"id="+this.productId+ Constants.SPACE_SMALL
                + "qty="+this.quantity+Constants.SPACE_SMALL
                + this.productName+Constants.SPACE_SMALL
                +this.discountMessage;
        return ret;
    }
}
