package com.sks.classes;


import com.sks.asbtracts.DecoratorOfBasicDiscount;
import com.sks.utils.Constants;
import java.util.ArrayList;

/**
 * handle a list of basic discounts
 */
public class BasicDiscountList {

    private ArrayList<DecoratorOfBasicDiscount> basicDiscountList=null;


    public BasicDiscountList(){
        this.basicDiscountList=new ArrayList<>(10);
    }

    public ArrayList<DecoratorOfBasicDiscount> getBasicDiscountList() {
        return basicDiscountList;
    }

    public void setBasicDiscountList(ArrayList<DecoratorOfBasicDiscount> basicDiscountList) {
        this.basicDiscountList = basicDiscountList;
    }

    public void addItem(DecoratorOfBasicDiscount decoratorOfBasicDiscount) {
        this.basicDiscountList.add(decoratorOfBasicDiscount);
    }


    /**
     * display BasicDiscountList
     */
    public void printBasicDiscountList(){
        System.out.println(Constants.LINE_SEPARATOR);
        System.out.println(Constants.SKIP_LINE+"BasicDiscount list"+ Constants.SKIP_LINE);
        basicDiscountList.stream().forEach(e-> System.out.println("product id "+e.getApplyOnProductId()+ Constants.SPACE_SMALL+e.getName()));
    }


}
